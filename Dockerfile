FROM gitlab/dind

ENV oc_version="v3.7.0"
ENV oc_version_hash="${oc_version}-7ed6862"

RUN cd /tmp \
  && apt-get update \
  && apt-get install -y wget \
  && wget https://github.com/openshift/origin/releases/download/${oc_version}/openshift-origin-client-tools-${oc_version_hash}-linux-64bit.tar.gz \
  && tar -xvzf openshift-origin-client-tools-${oc_version_hash}-linux-64bit.tar.gz \
  && mv openshift-origin-client-tools-${oc_version_hash}-linux-64bit/oc /usr/local/bin/ \
  && rm -rf openshift-origin-client-tools-${oc_version_hash}-linux-64bit openshift-origin-client-tools-${oc_version_hash}-linux-64bit.tar.gz

RUN cd /tmp \
  && apt-get install -y python3-pip \
  && python --version \
  && curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip" \
  && apt-get install unzip \
  && unzip awscli-bundle.zip \
  && sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws \
  && rm -rf awscli-bundle.zip

ADD selfca.crt /etc/ssl/certs/
